//
//  DinoPlayer.m
//  SideScrollerGame
//
//  Created by Vikas Koul on 21/03/17.
//  Copyright © 2017 Vikings. All rights reserved.
//

#import "DinoPlayer.h"

@implementation DinoPlayer
+(id)dinoPlayer{
//    DinoPlayer *hero = [DinoPlayer spriteNodeWithColor:[UIColor greenColor] size:CGSizeMake(20, 20)];
    DinoPlayer *hero = [DinoPlayer spriteNodeWithColor:[UIColor clearColor] size:CGSizeMake(20, 20)];
    //  DinoPlayer *hero = [DinoPlayer spriteNodeWithImageNamed:@"dino1_r.png"];
    //  SKCropNode * cropNode = [SKCropNode node];
    // SKSpriteNode *mask = [SKSpriteNode spriteNodeWithColor:[UIColor blackColor] size:CGSizeMake(hero.frame.size.width/2, hero.frame.size.height)];
    //    mask.position = CGPointMake(hero.frame.size.width/4, 0);
   // hero.name = @"hero";
    SKSpriteNode* dino = [SKSpriteNode spriteNodeWithImageNamed:@"dino1_r.png"];
    dino.size = hero.frame.size;
    //dino.position = CGPointMake(0, 0);
    [hero addChild:dino];
    hero.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:hero.size];
    [hero move];
    return hero;
}

-(void)move{
    SKAction *walkRight= [SKAction moveByX:10 y:0 duration:.005];
    [self runAction:walkRight];
}

-(void)jumpOverWildings{
    [self.physicsBody applyImpulse:CGVectorMake(3, 7)];
}
-(void)start{
    SKAction *moveRight = [SKAction moveByX:1 y:0 duration:0.005];
    SKAction *moveRightForever = [SKAction repeatActionForever:moveRight];
    [self runAction:moveRight];
}
@end

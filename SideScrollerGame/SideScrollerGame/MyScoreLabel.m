//
//  MyScoreLabel.m
//  SideScrollerGame
//
//  Created by Anshima on 23/03/17.
//  Copyright © 2017 Vikings. All rights reserved.
//

#import "MyScoreLabel.h"

@implementation MyScoreLabel
+(id)pointsLabel:(NSString *)fName{
    MyScoreLabel *pointsLabel = [MyScoreLabel labelNodeWithText:fName];
    pointsLabel.text =@"0";
    pointsLabel.name = @"scoreLabel";
    pointsLabel.score = 0;
    return pointsLabel;
}

-(void)incrementScore{
    self.score++;
    self.text = [NSString stringWithFormat:@"%i",self.score];
}
@end

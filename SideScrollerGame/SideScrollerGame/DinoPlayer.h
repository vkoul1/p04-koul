//
//  DinoPlayer.h
//  SideScrollerGame
//
//  Created by Vikas Koul on 21/03/17.
//  Copyright © 2017 Vikings. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

@interface DinoPlayer : SKSpriteNode
+(id)dinoPlayer;
-(void)move;
-(void)jumpOverWildings;
-(void)start;
@end

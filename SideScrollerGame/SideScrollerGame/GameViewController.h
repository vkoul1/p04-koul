//
//  GameViewController.h
//  SideScrollerGame
//
//  Created by Vikas Koul on 3/15/17.
//  Copyright © 2017 Vikings. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SpriteKit/SpriteKit.h>
#import <GameplayKit/GameplayKit.h>

@interface GameViewController : UIViewController

@end

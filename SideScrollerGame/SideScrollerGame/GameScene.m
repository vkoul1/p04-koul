//
//  GameScene.m
//  SideScrollerGame
//
//  Created by Vikas Koul on 3/15/17.
//  Copyright © 2017 Vikings. All rights reserved.
//

#import "GameScene.h"
#import "DinoPlayer.h"
#include "myWorld.h"
#include "MyScoreLabel.h"

@interface GameScene()
    @property BOOL hasGameStarted;
    @property BOOL hasGameEnded;
@end

@implementation GameScene {
    DinoPlayer *player;
    SKNode *world;
    myWorld *myworldgenerator;
}

- (void)didMoveToView:(SKView *)view {
    // Setup your scene here
    self.anchorPoint = CGPointMake(0.5, 0.5);
    self.backgroundColor = [SKColor colorWithRed:0.54 green:0.7853 blue:1.0 alpha:1.0];
    
    world = [SKNode node];
    [self addChild:world];
    
    SKSpriteNode* background = [SKSpriteNode spriteNodeWithImageNamed:@"bg.png"];
    //background.size = self.frame.size;
    background.position = CGPointMake(0, 0);
    [world addChild:background];
    
    myworldgenerator = [myWorld worldGenerator:world];
    [self addChild:myworldgenerator];
    [myworldgenerator populateWorld];
    
    //Adding the player to the scene
    player = [DinoPlayer dinoPlayer];
    [world addChild:player];
    
    MyScoreLabel *pointsLabel = [MyScoreLabel pointsLabel:@"Helvetica"];
    pointsLabel.position= CGPointMake(0, 75);
    [self addChild:pointsLabel];
    
}
-(void)didSimulatePhysics{
    [self centerOrNode:player];
    [self handleGeneration];
}
-(void)handleGeneration{
    [world enumerateChildNodesWithName:@"obstacle" usingBlock:^(SKNode * _Nonnull node, BOOL * _Nonnull stop) {
        if(node.position.x < player.position.x){
            node.name = @"obstacle_cancelled";
            [myworldgenerator generate];
            //Incrementing score
            MyScoreLabel *scoreLabel = (MyScoreLabel *)[self childNodeWithName:@"scoreLabel"];
            [scoreLabel incrementScore];
        }
    }];
}

-(void)centerOrNode:(SKNode *) node{
    CGPoint pos = [self convertPoint:node.position fromNode:node.parent];
    world.position = CGPointMake(world.position.x - pos.x, world.position.y);
}
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    if(!self.hasGameStarted){
        [player start];
        self.hasGameStarted = YES;
    }
    else if(self.hasGameEnded)
        [self clear];
    else
        [player jumpOverWildings];
}
-(void)start{
    NSLog(@"Start Method called");
    self.hasGameStarted = YES;
    [player start];
}
-(void)clear{
    NSLog(@"Clear Method called");
}
-(void)gameOver{
    NSLog(@"Game Over Method called");
    
}
-(void)update:(CFTimeInterval)currentTime {
    // Called before each frame is rendered
}

@end

//
//  MyScoreLabel.h
//  SideScrollerGame
//
//  Created by Anshima on 23/03/17.
//  Copyright © 2017 Vikings. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

@interface MyScoreLabel : SKLabelNode
@property int score;
+(id)pointsLabel:(NSString *)fName;
-(void)incrementScore;
@end

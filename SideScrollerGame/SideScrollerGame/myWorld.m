//
//  myWorld.m
//  SideScrollerGame
//
//  Created by Vikas Koul on 21/03/17.
//  Copyright © 2017 Vikings. All rights reserved.
//

#import "myWorld.h"

@interface myWorld()
@property double groundX;
@property double wildlingsX;
@property SKNode *world;
@end

int counter;

@implementation myWorld

+(id)worldGenerator:(SKNode *)world{
    myWorld *generator = [myWorld node];
    generator.groundX = 0;
    generator.wildlingsX = 300;
    generator.world = world;
    return generator;
}

-(void)populateWorld{
    for(int i=0;i<3;i++)
        [self generate];
}

-(void)generate{
    counter  = counter + 1;
    SKSpriteNode *ground = [SKSpriteNode spriteNodeWithColor:[UIColor blackColor] size:CGSizeMake(self.scene.frame.size.width, 100)];
    ground.name= @"ground";
    //Setting the ground position
    int screenHeightposition = -self.scene.frame.size.height/6 + ground.frame.size.height/2;
    //Adding physics body to the ground node
    ground.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:ground.size];
    ground.physicsBody.dynamic = NO;
    ground.position = CGPointMake(self.groundX, screenHeightposition);
    [self addChild:ground];
    self.groundX += ground.frame.size.width;
    
    //Adding humans wildlings to the game
    SKSpriteNode *humanObstacle = [SKSpriteNode spriteNodeWithColor:[UIColor clearColor] size:CGSizeMake(30, 30)];
    humanObstacle.name= @"obstacle";
    if(counter % 2 == 0){
        SKSpriteNode* wildling = [SKSpriteNode spriteNodeWithImageNamed:@"caveman1.png"];
        wildling.size = humanObstacle.frame.size;
        [humanObstacle addChild:wildling];
        humanObstacle.position = CGPointMake(self.wildlingsX, ground.position.y+ground.frame.size.height/4+humanObstacle.frame.size.height+5);
    }
    else{
        SKSpriteNode* wildling = [SKSpriteNode spriteNodeWithImageNamed:@"caveman2.png"];
        wildling.size = humanObstacle.frame.size;
        [humanObstacle addChild:wildling];
        humanObstacle.position = CGPointMake(self.wildlingsX, ground.position.y+ground.frame.size.height/4+humanObstacle.frame.size.height+10);
    }
    humanObstacle.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:humanObstacle.size];
    humanObstacle.physicsBody.dynamic = NO;
    [self.world addChild:humanObstacle];
    self.wildlingsX += 200;
}

@end

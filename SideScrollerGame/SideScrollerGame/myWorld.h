//
//  myWorld.h
//  SideScrollerGame
//
//  Created by Vikas Koul on 21/03/17.
//  Copyright © 2017 Vikings. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

@interface myWorld : SKNode
+(id)worldGenerator:(SKNode *)world;
-(void)populateWorld;
-(void)generate;
@end
